#!/bin/bash
# Get to location of root of the website:
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd ${SCRIPT_DIR}

TARGET_DIR=../locale/en_US.utf8/LC_MESSAGES/

# Ensure target dir
if ! [ -d "$DIRECTORY" ]; then
  mkdir -p ${TARGET_DIR}
fi

# Generate Pot:
# msgfilter -i en.po --keep-header -o en.pot true

for pofile in *.po ; do
mofile=${pofile/.po/.mo}
echo ==== Compiling $pofile to $mofile ====
msgfmt $pofile -o ${TARGET_DIR}/$mofile
done

echo Job list finished
