Carrot meets a tiger
========

* **Author:** @valvin
* **License:** [Creative Commons Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/)

## Scene 1

**Panel 1:** bird's eye angle over a city which looks like at what we can have in a country like India. In that city there is a zoo we can guess in the shot. 

**Panel 2:**  full shot on a street which leads to a zoo on which we can see Pepper is very happy but Carrot doesn't really want to go in that way. 
_Pepper : What a lovely idea to take breath in that city!_
        _and the visit of this zoo is exciting ! _
        _Come on Carrot ! Don't be afraid!_

**Panel 3:** mid shot on zoo's ticket office where Pepper can see that the office is closed because major has validated free zoo for everyone.
Carrot is still unhappy to go there.
_Pepper : Great idea! Everyone can now visit this zoo._

**Panel 4:** mid shot on a first cage where small dragons with many colors are playing like parrots. Now Carrot starts to take interest in this visit but tries to hide it. Pepper is reading carefully the explanation.
_Pepper : "Rainbow dragons are very rare and we can only see them in specific region where Réa is pure..."

**Panel 5:** Close-up shot on Carrot is now very excited and climb on a new cage where a strange red rabbit is present.

_Pepper : No Carrot ! Come down immediately !_
         _It is forbidden and red animals are very dangerous!_

## Scene 2

**Panel 6:** full shot - Pepper&Carrot walks inside the zoo. Carrots see an icescream merchant and tries to make Pepper approaching. But Pepper has seen something which seems to be more important.

_Pepper : Carrot ! You would love it !

**Panel 7:** low angle / Carrot's eye angle - Carrot is afraid because in the cage there is a big orange tiger which is approching the grids and looks effectively machiavellian.

**Panel 8:** close up on the tiger. We can still view Carrot who is scary. now the tiger looks Carrot in the eyes. His eyes are a bit red.

_Tiger : Carrot, _
        _I am your father!_ 

**Panel 9:** full shot - Pepper and Tiger laught loud together and Carrot is
making a unhappy mow and stick his tongue out.

## FIN
